<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Repository\ElementRepositoryInterface as RepositoryInterface;

class ElementResourceController extends BaseResourceController {

    public function __construct(RepositoryInterface $repository) {
        $this->repository = $repository;
    }

}
