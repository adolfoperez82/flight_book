<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Repository\GroupRepositoryInterface as RepositoryInterface;

class GroupResourceController extends BaseResourceController {

    public function __construct(RepositoryInterface $repository) {
        $this->repository = $repository;
    }

}
