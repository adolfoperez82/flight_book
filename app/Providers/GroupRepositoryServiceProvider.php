<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repository\GroupRepositoryInterface as RepositoryInterface;
use App\Repository\Implementations\GroupRepositoryImplementation as RepositoryImplementation;

class GroupRepositoryServiceProvider extends ServiceProvider
{

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
      $this->app->singleton(RepositoryInterface::class,  RepositoryImplementation::class);
  }
}
