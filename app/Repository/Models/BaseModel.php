<?php
namespace App\Repository\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseModel extends Model
{
    use SoftDeletes;
    protected $validationRulesNew;
    protected $validationRulesUpdate;

    public function getValidationRules($method){
        $method = strtolower($method);
        if($method === 'post'){
            return $this->validationRulesNew;
        } else if($method === 'put'){
            return $this->validationRulesUpdate;
        }
    }

}
