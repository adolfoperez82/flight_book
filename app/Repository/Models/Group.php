<?php
namespace App\Repository\Models;

class Group extends BaseModel
{
    protected $table = 'groups';
    protected $fillable = [
        'id', 'name'
    ];

    protected $validationRulesNew = [
        'name' => 'required|min:2|max:50'
    ];

    protected $validationRulesUpdate = [
        'name' => 'unique:groups|min:2|max:50'
    ];

    public function elements()
    {
        return $this->hasMany('\App\Repository\Models\Element', 'group_id', 'id');
    }

}
