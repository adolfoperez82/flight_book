<?php

use Illuminate\Database\Seeder;

class ElementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('elements')->insert([
            'name' => 'El Señor de los Anillos',
            'group_id' => 1
        ]);

        DB::table('elements')->insert([
            'name' => 'Camino del Exilio',
            'group_id' => 2
        ]);
    }
}
