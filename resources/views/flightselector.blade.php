<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="{{ asset('js/form.js') }}"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

        <!-- Styles -->
        <style>
          
        </style>
    </head>
    <body>
        <form class="book_form" action="bookPreview" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="departure" value="{{$flights['departure']}}"/>
            <input type="hidden" name="return" value="{{$flights['return']}}"/>
            <input type="hidden" name="oneWay" value="{{$flights['oneWay']}}"/>
            
            <input type="hidden" name="from" value="{{$flights['from']}}"/>
            <input type="hidden" name="to" value="{{$flights['to']}}"/>

            <input type="hidden" name="adults" value="{{$flights['adults']}}"/>
            <input type="hidden" name="children" value="{{$flights['children']}}"/>
            <input type="hidden" name="babies" value="{{$flights['babies']}}"/>

            <input type="hidden" name="out_hour" value=""/>
            <input type="hidden" name="out_price" value=""/>
            <input type="hidden" name="out_arrival_airport" value=""/>
            <input type="hidden" name="out_arrival_time" value=""/>
            <input type="hidden" name="ret_hour" value=""/>
            <input type="hidden" name="ret_price" value=""/>
            <input type="hidden" name="ret_arrival_airport" value=""/>
            <input type="hidden" name="ret_arrival_time" value=""/>

            <div class=" block clear">
                <h3>Outbound: {{$flights['out_date']}}</h3>
                <hr/>
                @foreach($flights['out'] as $out)
                <div class="line">
                    <div class="left_block">
                        <input type="radio" name="out_time" data-hour="{{$out['hour']}}" 
                        data-price="{{$out['price']}}" data-arrival_airport="{{$out['arrival_airport']}}" data-arrival_time="{{$out['arrival_time']}}"/>
                        {{$out['airport']}} {{$out['hour']}} -> {{$out['arrival_airport']}} {{$out['arrival_time']}}
                    </div>
                    <div class="right_block">
                        {{$out['price']}} €<br/>
                        Only {{$out['seats']}} available at {{$out['price']}}€
                    </div>
                    <div class="clear"></div>
                </div>                
                <hr/>
                @endforeach
                <div class="clear"></div>
            </div>
            @if(!$flights['oneWay'])
            <div class="block clear">
                <h3>Return: {{$flights['ret_date']}}</h3>
                <hr/>
                @foreach($flights['ret'] as $ret)
                <div class="line">
                    <div class="left_block">
                        <input type="radio" name="ret_time" data-hour="{{$ret['hour']}}"
                        data-price="{{$ret['price']}}" data-arrival_airport="{{$ret['arrival_airport']}}" data-arrival_time="{{$ret['arrival_time']}}"/>
                        {{$ret['airport']}} {{$ret['hour']}} -> {{$ret['arrival_airport']}} {{$ret['arrival_time']}}
                    </div>
                    <div class="right_block">
                        {{$ret['price']}} €<br/>
                        Only {{$ret['seats']}} available at {{$ret['price']}}€
                    </div>
                    <div class="clear"></div>
                </div>                
                <hr/>
                @endforeach
                <div class="clear"></div>
            </div>
            @endif
            <div class="block clear">
                <input type="submit" value="Preview"/>
            </div>
        </form>
    </body>
</html>
