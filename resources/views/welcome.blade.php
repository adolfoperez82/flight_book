<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <script src="{{ asset('js/form.js') }}"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
          
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <form class="flight_form" action="searchFlights" method="post">
                {{ csrf_field() }}
                <h3>Choose a Flight</h3>
                <p class="line"><label for="round_trip_one_way">Round trip Flight</label><input type="radio" name="round_trip_one_way" value="0" checked/></p>
                <p class="line"><label for="round_trip_one_way">One way</label><input type="radio" name="round_trip_one_way" value="1"/></p>
                <p class="line"><label for="departure">Departure:</label>
                    <select id="departure" name="departure">
                        <option value="">From...</option>
                        @foreach($departures as $code => $name)
                            <option value="{{$code}}">{{$name}}</option>
                        @endforeach
                    </select>
                </p>
                <p class="line"><label for="return">To:</label><select id="return" name="return"></select></p>
                
                <p class="line"><label for="from">From:</label><select id="from" name="from"></select></p>
                <p class="line round-trip"><label for="from">To:</label><select id="to" name="to"></select></p></p>
                <p class="line"><label for="adults">Adults:</label>
                    <select id="adults" name="adults">
                        @for($i=1;$i<10;$i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                </p>
                <p class="line"><label for="children">Children:</label>
                    <select id="children" name="children">
                        @for($i=0;$i<10;$i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                </p>
                <p class="line"><label for="babies">Babies:</label>
                    <select id="babies" name="babies">
                        @for($i=0;$i<10;$i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                </p>
                <p class="line">
                    <input type="submit" value="Search & book"/>
                </p>
            </form>
        </div>
    </body>
</html>
